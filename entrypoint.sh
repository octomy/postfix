#!/bin/sh

#
# This script will either start an postfix instance OR an OpenDKIM instance depending on the first argument
# If no arbument is provided, it will quit with an error showing usage
#
# The reason this architecture was chosen is to facilitate the following:
#  1. Ease of maintenance; it is easier to maintain one repo/docker/whatever than two
#  2. Keeping related config in one place; often changes in one will be reflectedin the other
#  3. Being able to sidecar opendkim  wihtout needing to pull a separate image
#

#####################################################################################################
# Info output
#####################################################################################################

APP="$0"
POSTMASTER_EMAIL="postmaster@merchbot.net"
POSTFIX_CONFIG_MAIN="/etc/postfix/main.cf"
POSTFIX_CONFIG_MASTER="/etc/postfix/master.cf"

OPENDKIM_CONFIG_MAIN="/etc/opendkim/opendkim.conf"
OPENDKIM_CONFIG_TRUSTED_HOSTS="/etc/opendkim/TrustedHosts"
OPENDKIM_CONFIG_KEY_TABLE="/etc/opendkim/KeyTable"
OPENDKIM_CONFIG_SIGNING_TABLE="/etc/opendkim/SigningTable"
OPENDKIM_CONFIG_DOMAIN_KEYS="/etc/opendkim/domainkeys"

APP_MODE="$1" || "postfix"
OPENDKIM_HOST=opendkim
OPENDKIM_PORT=12301
OPENDKIM_SELECTOR="mail"


echo "--- postfix startup --------"
echo "APP_MODE=                       $APP_MODE"
echo "SMTP_USER=                      $SMTP_USER"
echo "SMTP_DOMAIN=                    $SMTP_DOMAIN"
echo "POSTFIX_USER=                   $POSTFIX_USER"
echo "POSTFIX_PASSWORD=               $POSTFIX_PASSWORD"
echo "POSTMASTER_EMAIL=               $POSTMASTER_EMAIL"
echo "POSTFIX_CONFIG_MAIN=            $POSTFIX_CONFIG_MAIN"
echo "POSTFIX_CONFIG_MASTER=          $POSTFIX_CONFIG_MASTER"
echo "OPENDKIM_CONFIG_MAIN=           $OPENDKIM_CONFIG_MAIN"
echo "OPENDKIM_CONFIG_TRUSTED_HOSTS=  $OPENDKIM_CONFIG_TRUSTED_HOSTS"
echo "OPENDKIM_CONFIG_KEY_TABLE=      $OPENDKIM_CONFIG_KEY_TABLE"
echo "OPENDKIM_CONFIG_SIGNING_TABLE=  $OPENDKIM_CONFIG_SIGNING_TABLE"
echo "OPENDKIM_CONFIG_DOMAIN_KEYS=    $OPENDKIM_CONFIG_DOMAIN_KEYS"
echo "OPENDKIM_HOST=                  $OPENDKIM_HOST"
echo "OPENDKIM_PORT=                  $OPENDKIM_PORT"
echo "OPENDKIM_SELECTOR=              $OPENDKIM_SELECTOR"
echo "----------------------------"


#####################################################################################################
# Functions
#####################################################################################################



function print_config(){
    echo ""
    echo "------------------------------------------------------"
    echo "-- $1"
    echo "------------------------------------------------------"
    echo ""
    cat "$1"
    echo ""
}



function setup_postfix_basic(){
    # Start with empty config
    echo "# octomy-postfix-main\n--------------------------" > "$POSTFIX_CONFIG_MAIN"
    # Ditch legacy config format
    postconf compatibility_level=2
    # master already has config echo "# octomy-postfix-master\n--------------------------" > "$POSTFIX_CONFIG_MASTER"
    postconf -e myhostname=$SMTP_DOMAIN
    postconf -F '*/*/chroot = n'
    # Make postfix output to stdout so we don't need rsyslogd
    postconf -e maillog_file=/dev/stdout
    postconf -P "submission/inet/smtpd_recipient_restrictions=permit_sasl_authenticated,reject_unauth_destination"
}


function setup_postfix_sasl(){
    # SASL SUPPORT FOR CLIENTS
    # The following options set parameters needed by Postfix to enable
    # Cyrus-SASL support for authentication of mail clients.
    # /etc/postfix/main.cf
    postconf -e smtpd_sasl_auth_enable=yes
    postconf -e broken_sasl_auth_clients=yes
    postconf -e smtpd_recipient_restrictions=permit_sasl_authenticated,reject_unauth_destination
    
    # smtpd.conf
    cat >> /etc/postfix/sasl/smtpd.conf <<EOF
pwcheck_method: auxprop
auxprop_plugin: sasldb
mech_list: PLAIN LOGIN CRAM-MD5 DIGEST-MD5 NTLM

EOF
    
    # sasldb2
    echo $SMTP_USER | tr , \\n > /tmp/passwd
    while IFS=':' read -r POSTFIX_USER POSTFIX_PASSWORD; do
        echo $POSTFIX_PASSWORD | saslpasswd2 -p -c -u $SMTP_DOMAIN $POSTFIX_USER
    done < /tmp/passwd
    
    chown postfix.sasl /etc/sasldb2
    postconf -P "submission/inet/smtpd_sasl_auth_enable=yes"
}


function setup_postfix_tls(){
    if [[ -n "$(find /etc/postfix/certs -iname *.crt)" && -n "$(find /etc/postfix/certs -iname *.key)" ]]; then
        echo "FOUND certificates, enabling TLS"
        postconf -e smtpd_tls_cert_file=$(find /etc/postfix/certs -iname *.crt)
        postconf -e smtpd_tls_key_file=$(find /etc/postfix/certs -iname *.key)
        chmod 400 /etc/postfix/certs/*.*
    
        postconf -M submission/inet="submission   inet   n   -   n   -   -   smtpd"
        postconf -P "submission/inet/syslog_name=postfix/submission"
        postconf -P "submission/inet/smtpd_tls_security_level=encrypt"
        postconf -P "submission/inet/milter_macro_daemon_name=ORIGINATING"
    else
        echo "Certificates not found, skipping TLS setup"
    fi
}



function setup_postfix_dkim(){
    postconf -e milter_protocol=2
    postconf -e milter_default_action=accept
    postconf -e smtpd_milters=inet:$OPENDKIM_HOST:$OPENDKIM_PORT
    postconf -e non_smtpd_milters=inet:$OPENDKIM_HOST:$OPENDKIM_PORT
}



function setup_opendkim_basic(){
    DOMAIN_KEYS_DIR="$OPENDKIM_CONFIG_DOMAIN_KEYS/$SMTP_DOMAIN"
    DOMAIN_PRIVATE_KEY="$DOMAIN_KEYS_DIR/$OPENDKIM_SELECTOR.private"
    DOMAIN_TXT="$DOMAIN_KEYS_DIR/$OPENDKIM_SELECTOR.txt"
    
    if [ ! -f "$DOMAIN_PRIVATE_KEY" ]; then
        echo "OpenDKIM did not find existing private key, generating..."
        mkdir -p "$DOMAIN_KEYS_DIR"
        opendkim-genkey \
            --verbose \
            --bits 4096 \
            --directory "$DOMAIN_KEYS_DIR" \
            --selector "$OPENDKIM_SELECTOR" \
            --domain "$SMTP_DOMAIN"

        if [ -f "$DOMAIN_PRIVATE_KEY" ]; then
            echo "OpenDKIM generated private key: $DOMAIN_PRIVATE_KEY successfully!"
            print_config "$DOMAIN_TXT"
            chown opendkim:opendkim "$DOMAIN_PRIVATE_KEY"
            chmod 0400 "$DOMAIN_PRIVATE_KEY"
        else
            echo "OpenDKIM failed to generate private key: $DOMAIN_PRIVATE_KEY, aborting..."
            exit 2
        fi
    else
        echo "OpenDKIM found existing private key: $PRIVATE_KEYS, skipping keys generation"
    fi

    cat > "$OPENDKIM_CONFIG_MAIN" <<EOF
AutoRestart             Yes
AutoRestartRate         10/1h
UMask                   002
BaseDirectory           /run/opendkim
LogWhy                  Yes
#Syslog                  yes
#SyslogSuccess           Yes

Canonicalization        relaxed/simple

ExternalIgnoreList      refile:$OPENDKIM_CONFIG_TRUSTED_HOSTS
InternalHosts           refile:$OPENDKIM_CONFIG_TRUSTED_HOSTS
KeyTable                refile:$OPENDKIM_CONFIG_KEY_TABLE
SigningTable            refile:$OPENDKIM_CONFIG_SIGNING_TABLE

Mode                    sv
PidFile                 /var/run/opendkim/opendkim.pid
SignatureAlgorithm      rsa-sha256

UserID                  opendkim:opendkim

Socket                  inet:$OPENDKIM_PORT@localhost

Domain                  $SMTP_DOMAIN
Selector                $OPENDKIM_SELECTOR
KeyFile                 $DOMAIN_PRIVATE_KEY

ReportAddress           $POSTMASTER_EMAIL
SendReports             yes

EOF
}



function setup_opendkim_trusted_hosts(){
    cat >> "$OPENDKIM_CONFIG_TRUSTED_HOSTS" <<EOF
127.0.0.1
localhost
192.168.0.1/24

postfix
*.$SMTP_DOMAIN
$SMTP_DOMAIN

EOF
}


function setup_opendkim_key_table(){
    DOMAINKEYS=$(find "$OPENDKIM_CONFIG_DOMAIN_KEYS" -iname *.private)
    cat >> "$OPENDKIM_CONFIG_KEY_TABLE" <<EOF
mail._domainkey.$SMTP_DOMAIN $SMTP_DOMAIN:mail:$DOMAINKEYS

EOF
}


function setup_opendkim_signing_table(){
    cat >> "$OPENDKIM_CONFIG_SIGNING_TABLE" <<EOF
*@$SMTP_DOMAIN mail._domainkey.$SMTP_DOMAIN

EOF
}



function print_postfix_config(){
    print_config "$POSTFIX_CONFIG_MAIN"
    print_config "$POSTFIX_CONFIG_MASTER"
}

function print_opendkim_config(){
    print_config "$OPENDKIM_CONFIG_MAIN"
    print_config "$OPENDKIM_CONFIG_TRUSTED_HOSTS"
    print_config "$OPENDKIM_CONFIG_KEY_TABLE"
    print_config "$OPENDKIM_CONFIG_SIGNING_TABLE"
}

function run_postfix(){
    echo "Starting Postfix in foreground:"
    exec "/usr/sbin/postfix" "start-fg"
}

function run_opendkim(){
    /usr/sbin/opendkim -n
    echo "Starting OpenDKIM in foreground:"
    exec "/usr/sbin/opendkim" "-f"
}


#####################################################################################################
# Entrypoints
#####################################################################################################


do_postfix(){
    echo "Starting in Postfix mode"
    setup_postfix_basic
    # Disabled for now as we don't have any clients (we are send only smtp instance)
    # setup_postfix_sasl
    setup_postfix_tls
    setup_postfix_dkim
    print_postfix_config
    #postconf -d
    run_postfix
}

do_opendkim(){
    echo "Starting in OpenDKIM mode"
    setup_opendkim_basic
    setup_opendkim_trusted_hosts
    setup_opendkim_key_table
    setup_opendkim_signing_table
    print_opendkim_config
    run_opendkim
}


do_usage(){
#    echo "Mode'$APP_MODE' was not valid!\n\tUsage $APP postfix # Configure and start Postfix instance\n\t $APP opendkim # Configure and start OpenDKIM instance\n\nAborting..."
    res=$(cat <<EOF
    Mode '$APP_MODE' was not valid!

    Usage:

        $APP postfix    # Configure and start Postfix instance

        $APP opendkim   # Configure and start OpenDKIM instance

    Aborting...

EOF
)
    echo "$res"
    exit 1
}




#####################################################################################################
# Mode selection
#####################################################################################################


# Determine what mode we are in and proceed
case $APP_MODE in
    postfix)
        do_postfix
    ;;
    
    opendkim)
        do_opendkim
    ;;
    
    *)
        do_usage
    ;;
esac
