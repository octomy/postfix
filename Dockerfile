# syntax = docker/dockerfile:1.0-experimental
ARG USER_NAME=fk
ARG USER_ID=8888
ARG HOME=/app
ARG VIRTUAL_ENV=${HOME}/fk_venv
ARG BUILD_DATE
ARG VCS_REF
ARG BUILD_VERSION


################################################################################
################################################################################
FROM alpine:3.13 as opendkim-build
################################################################################

ARG USER_NAME
ARG USER_ID
ARG HOME
ARG VIRTUAL_ENV
ARG VCS_REF
ARG BUILD_VERSION

ENV USER_NAME ${USER_NAME}
ENV USER_ID ${USER_ID}
ENV HOME ${HOME}
ENV VIRTUAL_ENV ${VIRTUAL_ENV}

RUN echo "USER_NAME=  ${USER_NAME}"; \
    echo "USER_ID=    ${USER_ID}"; \
    echo "HOME=       ${HOME}"; \
    echo "VIRTUAL_ENV=${VIRTUAL_ENV}"

RUN mkdir -p "$HOME"

RUN addgroup \
    --gid "${USER_ID}" \
    --system \
    "${USER_NAME}"

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "${HOME}" \
    --ingroup "${USER_NAME}" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${USER_ID}" \
    "${USER_NAME}"

RUN chown -R "${USER_ID}:${USER_ID}" "${HOME}"

# preparing the workspace
RUN set -xe; \
    apk update; \
    apk add --virtual .build alpine-sdk git sudo

RUN set -xe; \
    adduser -D build; \
    addgroup build abuild; \
    echo 'build ALL=(ALL) NOPASSWD: /bin/mkdir, /bin/cp' > /etc/sudoers.d/build; \
    sudo -u build abuild-keygen -a -n -i; \
    rm /etc/sudoers.d/build

COPY --chown=8888:8888 ./opendbx /app/opendbx

# building opendbx
RUN set -xe; \
    chown -R build /app; \
    cd /app/opendbx; \
    chmod -R 777 /app/opendbx; \
    sudo -u build abuild checksum && sudo -u build abuild -r

# fetching the APKBUILD for opendkim
RUN set -xe; \
    cd /tmp; \
    sudo -u build git init; \
    sudo -u build git remote add origin -f git://git.alpinelinux.org/aports; \
    sudo -u build git config core.sparsecheckout true; \
    echo "community/opendkim/*" >> .git/info/sparse-checkout; \
    sudo -u build git pull origin 3.13-stable

# patching opendkim
RUN set -xe; \
    echo '/home/build/packages/app' >> /etc/apk/repositories; \
    apk update; \
    mv /tmp/community/opendkim /app/opendkim; \
    cd /app/opendkim/; \
    sed -i -e 's/\(sysconfdir.*\)/\1 \\/' -e '/sysconfdir/a \\t\t--with-odbx' APKBUILD; \
    sed -i 's/\(makedepends="\)/\1opendbx-dev /' APKBUILD

# building opendkim
RUN set -xe; \
    cd /app/opendkim/; \
    apkgrel -a .; \
    sudo -u build abuild checksum && sudo -u build abuild -r

USER $USER_ID
WORKDIR $HOME

################################################################################
################################################################################
FROM alpine:3.13
################################################################################

ARG USER_NAME
ARG USER_ID
ARG HOME
ARG VIRTUAL_ENV
ARG VCS_REF
ARG BUILD_VERSION

ENV USER_NAME ${USER_NAME}
ENV USER_ID ${USER_ID}
ENV HOME ${HOME}
ENV VIRTUAL_ENV ${VIRTUAL_ENV}

RUN mkdir -p "$HOME"

RUN addgroup \
    --gid "${USER_ID}" \
    --system \
    "${USER_NAME}"

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "${HOME}" \
    --ingroup "${USER_NAME}" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${USER_ID}" \
    "${USER_NAME}"

RUN chown -R "$USER_ID:$USER_ID" "$HOME"

COPY --from=opendkim-build /home/build/packages/app /tmp/pkgs
COPY --from=opendkim-build /etc/apk/keys/build* /etc/apk/keys/

RUN set -xe; \
    echo '/tmp/pkgs' >> /etc/apk/repositories; \
    apk update; \
    apk add opendkim; \
    rm -R /tmp/pkgs; \
    install -d -o opendkim -g opendkim /run/opendkim

RUN apk add --no-cache postfix postfix-pcre opendkim-utils
RUN echo "maillog_file = /dev/stdout" >> /etc/postfix/main.cf

#
#USER $USER_ID
WORKDIR $HOME

# Add files
COPY --chown=8888:8888 entrypoint.sh .

ENTRYPOINT ["/app/entrypoint.sh"]
#CMD ["/app/entrypoint.sh", "opendkim"]
#CMD cd /opt && ls  -halt
#install.sh

# https://medium.com/@chamilad/lets-make-your-docker-image-better-than-90-of-existing-ones-8b1e5de950d
LABEL maintainer="Lennart Rolland <lennart@octomy.org>"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.version=$BUILD_VERSION
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.name="octomy/postfix"
LABEL org.label-schema.description="Minimal send only postfix on alpine with TLS, DKIM & SPF"
LABEL org.label-schema.url="http://octomy.org"
LABEL org.label-schema.vcs-url="https://gitlab.com/octomy/postfix"
LABEL org.label-schema.vendor="octomy"
